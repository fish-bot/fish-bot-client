import { Device } from "./device";

describe("device", () => {
  it("should work", () => {
    const device = new Device("device-1", "test-device");

    device.toggleRelay(1, true);
    expect(device.isRelayTurnedOn(1)).toBe(true);
    device.destroy();
  });
});
