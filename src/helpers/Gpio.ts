import { Gpio as GpioBase } from "onoff";

export class Gpio extends GpioBase {

  /**
   * Those static are present in GpioBase, but types/onoff have not declared it yet
   */
  static accessible: boolean;
  static HIGH: 1;
  static LOW: 0;
}
