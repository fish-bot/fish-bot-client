import { DeviceControlViewModel, DeviceViewModel } from "./types";
import { Device } from "./device";

export function mapDevice(device: Device): DeviceViewModel {
  const controls = [
    mapDeviceRelayControl(device, 1),
    mapDeviceRelayControl(device, 2),
    mapDeviceRelayControl(device, 3),
    mapDeviceRelayControl(device, 4),
    mapDeviceRelayControl(device, 5),
    mapDeviceRelayControl(device, 6),
    mapDeviceRelayControl(device, 7),
    mapDeviceRelayControl(device, 8),
  ];

  return {
    id: device.id,
    name: device.name,
    controls
  }
}

export function mapDeviceRelayControl(device: Device, relayId: number): DeviceControlViewModel {
  return {
    id: `${relayId}`,
    name: `Реле ${relayId}`,
    type: "relay",
    properties: {
      turned_on: device.isRelayTurnedOn(relayId),
      synced: true
    }
  }
}
