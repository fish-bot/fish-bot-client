export interface Config {
  DEVICE_ID: string;
  DEVICE_NAME: string;
  SERVER: string;
  API_KEY: string;
}

export interface DeviceViewModel {
  id: string;
  name: string;
  controls: DeviceControlViewModel[];
}

export enum DeviceControlTypes {
  Relay = "relay"
}

export interface DeviceControlViewModel {
  id: string;
  name: string;
  type: string;
  properties: DeviceControlPropertiesViewModel;
}

export interface DeviceControlPropertiesViewModel {
  turned_on?: boolean;
  synced?: boolean;
}

export interface DeviceControl {
  id: string;
  name: string;
  type: DeviceControlTypes;
  properties: DeviceControlProperties;
}

export interface DeviceControlProperties {
  turnedOn?: boolean;
  synced?: boolean;
}


export interface Message {
  type: "push" | "response";
  action: string;
  payload?: object;
  error?: string;
}