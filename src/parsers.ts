import { DeviceControlProperties } from "./types";

interface IUpdatedDeviceControlProperties {
  deviceId: string;
  controlId: string;
  properties: DeviceControlProperties
}

export function parseUpdatedDeviceControlProperties(payload: any): IUpdatedDeviceControlProperties {
  const deviceId = `${payload && payload.device_id}`;
  const controlId = `${payload && payload.control_id}`;
  const properties: DeviceControlProperties = {};

  if (payload && payload.properties && payload.properties.turned_on != null) {
    properties.turnedOn = !!payload.properties.turned_on;
  }

  if (payload && payload.properties && payload.properties.synced != null) {
    properties.synced = !!payload.properties.synced;
  }

  return {
    deviceId,
    controlId,
    properties
  }
}