import { parseUpdatedDeviceControlProperties } from "./parsers";

test("parseUpdatedDeviceControlProperties", () => {
  expect(parseUpdatedDeviceControlProperties({ device_id: 'pi-d-1', control_id: '2', properties: { turned_on: true, synced: false } })).toEqual({
    deviceId: "pi-d-1",
    controlId: "2",
    properties: {
      turnedOn: true,
      synced: false
    }
  })
});
