import * as dotenv from "dotenv";
import { Config } from "./types";

const result = dotenv.config();

if (result.error) {
  console.log("dotenv.config error: ", result.error);
}

if (!process.env.API_KEY) {
  console.log("process.env.API_KEY is required");
}

if (!process.env.SERVER) {
  console.log("process.env.SERVER is required");
}

if (!process.env.DEVICE_ID) {
  console.log("process.env.DEVICE_ID is required");
}

export const config: Config = {
  API_KEY: process.env.API_KEY || "ABC-XYZ",
  SERVER: process.env.SERVER || "http://localhost:3000",
  DEVICE_ID: process.env.DEVICE_ID || "1",
  DEVICE_NAME: process.env.DEVICE_NAME || "Device"
};
