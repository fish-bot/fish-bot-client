import * as io from "socket.io-client";
import * as Debug from "debug";
import { config } from "./config";
import { mapDevice } from './mappers';
import { parseUpdatedDeviceControlProperties } from "./parsers";
import { Device } from "./device";
import { DeviceControlPropertiesViewModel } from "./types";

const debug = Debug("app");
const device = new Device(config.DEVICE_ID, config.DEVICE_NAME);

debug('connecting to server: %s', config.SERVER);

const socket = io(config.SERVER, {
  transports: ["websocket", "polling"]
});

socket.on("connect", onConnect);
socket.on("message", deviceIncomingEvent)
socket.on("disconnect", deviceOnDisconnect);
socket.on("connect_error", onConnectError);
socket.on("connect_timeout", onConnectTimeout);
socket.on("error", onSocketError);
socket.on("reconnect", onSocketReconnect);
socket.on("reconnect_attempt", onSocketReconnectAttempt);
socket.on("reconnect_error", onSocketReconnectError);
socket.on("reconnect_failed", onSocketReconnectFailed);
socket.on("reconnecting", onSocketReconnecting);

process.on("SIGINT", () => {
  device.destroy();
  socket.disconnect();
  process.exit();
});

function onConnect() {
  debug('device is connected. emit login actions');

  socket.emit("message", {
    action: 'login',
    payload: {
      api_key: config.API_KEY,
      device: mapDevice(device)
    }
  })
}

function deviceIncomingEvent(data) {
  debug("incomming event(action = %s, data = %o)", data.action, data.payload);

  if (data.action == "login") {
    return debug('device logged in');
  }

  if (data.action == "updated_device_control_properties") {
    const { deviceId, controlId, properties } = parseUpdatedDeviceControlProperties(data.payload)

    if (deviceId !== device.id) {
      return debug("ignore: `device_id` do not match to current `device.id`");;
    }

    if (!properties.synced) {
      debug("we have un-synced changes to apply: %o", properties);

      try {
        device.toggleRelay(parseInt(controlId, 10), properties.turnedOn);

        socket.emit("message", {
          action: "update_device_control_properties",
          payload: {
            device_id: deviceId,
            control_id: controlId,
            properties: {
              synced: true
            } as DeviceControlPropertiesViewModel
          }
        })
      } catch (error) {
        debug("error updating controlId=%s, properties=%o", controlId, properties);
      }
    }

    return;
  }

  return debug('unknown message', data);
}

function deviceOnDisconnect() {
  debug('device disconnect');
}

function onConnectError(error) {
  debug("on connect error: %s", error);
}

function onConnectTimeout(timeout) {
  debug("on connect timeout: %s", timeout);
}

function onSocketError(error) {
  debug("on socket error: %s", error);
}

function onSocketReconnect(attemptNumber) {
  debug("on socket reconnect, attempt number=%s", attemptNumber);
}

function onSocketReconnectAttempt(attemptNumber) {
  debug("on socket reconnect attempt, attempt number=%s", attemptNumber);
}

function onSocketReconnectError(error) {
  debug("on socket reconnect error, %s", error);
}

function onSocketReconnectFailed() {
  debug("on socket reconnect failed");
}

function onSocketReconnecting(attemptNumber) {
  debug("on socket reconnecting, attempt number=%s", attemptNumber);
}
