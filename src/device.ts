import { Gpio } from "./helpers/Gpio";
import * as Debug from "debug";
import { ok } from "assert";

if (!Gpio.accessible) {
  console.error(new Error("Gpio is unavailable"));
}

export class Device {
  id: string;
  name: string;

  relay1: Gpio;
  relay2: Gpio;
  relay3: Gpio;
  relay4: Gpio;
  relay5: Gpio;
  relay6: Gpio;
  relay7: Gpio;
  relay8: Gpio;

  constructor(id: string, name: string) {
    this.id = id;
    this.name = name;
    this.relay1 = new Gpio(2, "high");
    this.relay2 = new Gpio(3, "high");
    this.relay3 = new Gpio(4, "high");
    this.relay4 = new Gpio(17, "high");
    this.relay5 = new Gpio(27, "high");
    this.relay6 = new Gpio(22, "high");
    this.relay7 = new Gpio(10, "high");
    this.relay8 = new Gpio(9, "high");
  }

  toggleRelay(relayId: number, turnedOn: boolean) {
    this.getRelayById(relayId).writeSync(turnedOn ? 0 : 1);
  }

  isRelayTurnedOn(relayId: number): boolean {
    return this.getRelayById(relayId).readSync() ? false : true;
  } 

  destroy() {
    this.relay1.unexport();
    this.relay2.unexport();
    this.relay3.unexport();
    this.relay4.unexport();
    this.relay5.unexport();
    this.relay6.unexport();
    this.relay7.unexport();
    this.relay8.unexport();
  }

  private getRelayById(relayId): Gpio {
    const relay = this[`relay${relayId}`] as Gpio;

    ok(relay, "Unknown relayId")

    return relay;
  }
}
